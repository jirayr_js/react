import {useState} from "react";

function Header() {
    const [x, setX] = useState(0);

    function update() {
        setX(x + 1);
    }

    return (
        <div id="header">
            header {x}
            <button onClick={update}>update header component</button>
        </div>
    );
}

export default Header;
