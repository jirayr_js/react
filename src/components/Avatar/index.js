import {memo, useEffect, useState} from "react";

function Avatar(props) {
    const [a, setA] = useState(true);
    const [b, setB] = useState(55);

    useEffect(() => {
        console.log("111111111111")
    });

    useEffect(() => {
        console.log("888888888888")
    }, [a]);

    console.log("222222222222222")
    return (
        <div>
            <span>{a}</span>
            <br/>
            <span>{b}</span>
            <br/>
            <button onClick={() => setA(!a)}>update Avatar {a}</button>
            ------------------------
        </div>
    );
}

export default memo(Avatar);
