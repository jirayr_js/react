import Avatar from "../Avatar";
import {useEffect} from "react";

function App() {
    return (
        <div id="app">
            <Avatar />
        </div>
    );
}

export default App;
